import React, {useEffect, useState} from 'react';
import { BasicTable } from './TableComponent';

/* Helper function to get date from 1 month ago */
const getDateStringLastMonth = () => {
    var d = new Date();
    d.setMonth(d.getMonth() - 1)
    return d.toISOString().split('T')[0]
}

/*
    Formats repository data into a 2D array with desired columns for table display
    [id, name, desc, num_stars]
*/
const formatRepoData = (repoObj) => {
    return repoObj.items.map((item) => ([
        item.id,
        item.name,
        item.description,
        item.stargazers_count
    ]))
}

/*
    Fetches the top 5 repos created in the last month, sorted by number of stars
*/
export const ReposTable = () => {
    const date = getDateStringLastMonth()
    const query = 'https://api.github.com/search/repositories?' + encodeURI(`q=created:>${date}&per_page=5&sort=stars`);
    const [repoData, setRepoData] = useState([[]])
    const [refresh, setRefresh] = useState(false)

    useEffect(() => {
        fetch(query)
        .then(response => {
            return response.json()
        })
        .then(data => setRepoData(formatRepoData(data)))
    }, [setRepoData, refresh])

    return (
        <div id="repos-table">
            <BasicTable columns={['id', 'name', 'description', 'stars']} data={repoData} />
            <button id="hot_repo" onClick={() => setRefresh(!refresh)}>Refresh Repos</button>
        </div>
    )
}

