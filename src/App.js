import logo from './logo.svg';
import './App.css';
import { UsersTable } from './UsersTable'
import { ReposTable } from './ReposTable'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <ReposTable />
        <UsersTable />
      </header>
    </div>
  );
}

export default App;
