import React, { useEffect, useState } from 'react'
import { BasicTable } from './TableComponent'
import parseLinkHeader from 'parse-link-header'

/* Helper function to get date from 1 year ago */
const getDateStringLastYear = () => {
    var d = new Date();
    d.setFullYear(d.getFullYear() - 1)
    return d.toISOString().split('T')[0]
}

/*
    Computes the total number of followers through pagination links
*/
const FollowersCount = ({ followers_url, refresh }) => {
    const [followerCount, setFollowerCount] = useState(0)
    useEffect(() => {
        fetch(followers_url)
        .then((response) => {
            const linkHeader = parseLinkHeader(response.headers.get('link'))
            return Promise.all([fetch(linkHeader.last.url).then((res) => res.json()), Promise.resolve(linkHeader.last.page)])
        })
        .then(([lastPageData, totalNumPages]) => {
            // Compute the total number of followers
            const defaultPageSize = 30
            const pagesMinusLast = totalNumPages - 1
            setFollowerCount(lastPageData.length + (defaultPageSize * pagesMinusLast))
        })
    }, [refresh])
    return followerCount
}

const Avatar = ({ avatar_url }) => {
    return <img src={avatar_url} width="50" height="50"/>
}

/*
    Format user data for display in table
*/
const formatUserData = (userObj, refresh) => {
    return userObj.items.map((item) => ([
        item.id,
        item.login,
        <Avatar avatar_url={item.avatar_url} />,
        <FollowersCount followers_url={item.followers_url} refresh={refresh} />
    ]))
}

/*
    Displays the top five most followed github users created in the past year
*/
export const UsersTable = () => {
    const date = getDateStringLastYear()
    const [userData, setUserData] = useState([[]])
    const [refresh, setRefresh] = useState(false)
    const resultsPerPage = 5
    const query = 'https://api.github.com/search/users?' + encodeURI(`q=created:>${date}&per_page=${resultsPerPage}&sort=followers`);

    useEffect(() => {
        fetch(query)
        .then(response => {
            return response.json()
        })
        .then((userData) => {
            setUserData(formatUserData(userData, refresh))
        })
        const refreshTimer = setInterval(() => {
            setRefresh(!refresh)
        }, 120000) // 2 minutes
        return () => clearInterval(refreshTimer)
    }, [setUserData, refresh])

    return (<div id="users-table">
            <BasicTable columns={['id', 'login', 'avatar', 'followers']} data={userData} />
            <button id="prolific_users" onClick={() => setRefresh(!refresh)}>Refresh Users</button>
        </div>)

}