import React from 'react';
/*
    row: n size array of items to display in a row
*/
const TableRow = ({row}) => {
    return (
        <tr>
            {row.map(item => <td>{item}</td>)}
        </tr>
    )
}

/* 
    Displays tabular data using an html table
    columns: n size array of column headers
    data: n*m array of tabular data
*/
export const BasicTable = ({columns, data}) => {

    return (
        <div>
            <table>
                <thead>
                    <tr>
                        {columns.map(col => <th>{col}</th>)}
                    </tr>
                </thead>
                <tbody>
                    {data.map(row => <TableRow row={row} />)}
                </tbody>
            </table>
        </div>
    )
}